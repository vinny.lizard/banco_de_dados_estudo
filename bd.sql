CREATE TABLE CLIENTE ( -- CRIA A TABELA CLIENTE
	Cod_Cliente int not null, -- CRIA O CAMPO COD_CLIENTE E DEFINE O CAMPO COMO INTEIRO E PREENCHIMENTO OBRIGATORIO
	nome varchar(50),
	email varchar (60),
	telefone varchar (50),
	cod_cidade int,
	primary key (Cod_Cliente) -- DEFINE A CHAVE PRIMARIA DA TABELA
	
	); 

CREATE TABLE CIDADE( -- CRIA A TABELA CIDADE
	cod_cidade int not null,-- CRIA O ATRIBUTO E DEFINE O SEU TIPO E SE O CAMPO É OBRIGATÓRIO
	nome varchar (50) -- CRIA UM CAMPO DO TIPO CARACTERE COM A CAPACIDADE DE ARMAZENAR 50 ELEMENTOS
	
	);

alter table cidade add constraint pk_cidade --ALTERAR A TABELA CIDADE E DEFINIR A RESTRIÇÃO, DEFININDO A CHAVE PRIMARIA
primary key (cod_cidade);


alter table cliente			-- ALTERA A TABELA CLIENTE
	add constraint fk_cidade -- ADICIONA A RESTRIÇÃO
	foreign key (cod_cidade) -- DEFINE A CHAVE ESTRANJEIRA - E DEFINE O CAMPO DA TABELA QUE ESTÁ SENDO ALTERADA 
	references cidade; -- FAZ A REFERENCIA DA CHAVE PRIMARIA A COD_CIDADE

insert into cidade (cod_cidade, nome) -- COMANDO INSERE DADOS NA TABELA CIDADE, E DEFINE OS CAMPOS QUE DEVEM SER ALTERADOS
	values (1, 'FEIRA DE SANTANA'),--VALUES SE REFERE AO VALOR QUE VAI SER ATRIBUIDO AO CAMPO
	(2,'SALVADOR'),
	(3,'TANQUINHO'),
	(4,'IRARÁ'),
	(5,'SANTA BÁRBARA'),
	(6,'COITÉ')
	;

select *from cidade;
select *from cliente;

insert into cliente (cod_cliente, nome, email, telefone,cod_cidade) --  INSERIR DADOS NA TABELA CLIENTE 
values(1,'VINICIUS LOPES', 'vinny@unifacs.br','75 9 9990-9999',5); -- INSERE VALOR NAS TABELAS


insert into cliente (cod_cliente, nome, email, telefone,cod_cidade)
values (2,'JUSSARA','jussarasiparck@drogas.br','75 9 1233-0098',1);

insert into cliente (cod_cliente, nome, email, telefone,cod_cidade)
values (3,'STUART LITTLE','piratasdocaribe@stuartlittle.br','21 9 0560-0012',3);

insert into cliente
values(4, 'LOPES','lopes@live.com','75 9 9909-0912',4);

insert into cliente (cod_cliente, nome)
values (5,'GUSTAVO');

insert into cliente
values (6,'CALANGO');

update cliente set email = 'gustavo@loes.csdasd', telefone = '75 9 9909-0912', -- FAZ ALTERAÇÃO NA TABELA CLIENTE, ALTERANDO OS CAMPOS INDICADOS ENTRE PARENTESES
cod_cidade = 2 where cod_cliente = 5; -- ALTERA OS CAMPOS ONDE ESTÁ REFERENCIADO O COD_CLIENTE ' 5 '

select cliente.nome, cidade.nome -- SELECIONA NA TABELA CLIENTE O CAMPO NOME, E NA TABELA CIDADE O CAMPO NOME,
from cliente, cidade	-- SELECIONA AS TABELAS
where cliente.cod_cidade = cidade.cod_cidade; -- FAZ UMA COMPARAÇÃO ENTRE O CLIENTE COD_CIDADE ONDE CIDADE COD_CIDADE

select *
from cliente, cidade
where cliente.cod_cliente = cidade.cod_cidade
and cidade.cod_cidade = 2;

select *from cliente inner join cidade -- FAZ A COMPARAÇÃO ENTRE AS DUAS TABELAS SEM A NECESSIDADE DE EFETUAR O PLANO CARTEZIANO
on(cidade.cod_cidade = cliente.cod_cidade);

select * from cidade;

select * from cidade left join cliente
on (cidade.cod_cidade = cliente.cod_cidade);


select * from cliente right join cidade
on (cidade.cod_cidade = cliente.cod_cidade);

select max(cod_cliente) from cliente; -- VALOR MAXIMO
select min (cod_cliente) from cliente; -- VALOR MINIMO

select avg(cod_cliente) from cliente; -- MEDIA ENTRE OS VALORES

select count(*)from cliente; -- QUANTIDADE DE LINHAS

select cod_cidade, count(*) from cliente -- RETORNA A QUANTIDADE DE GERAL NA CONSULTA 
group by cod_cidade;
